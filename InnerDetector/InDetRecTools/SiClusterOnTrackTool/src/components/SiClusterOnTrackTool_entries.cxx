#include "SiClusterOnTrackTool/PixelClusterOnTrackTool.h"
#include "SiClusterOnTrackTool/SCT_ClusterOnTrackTool.h"
#include "SiClusterOnTrackTool/ITkPixelClusterOnTrackTool.h"

DECLARE_COMPONENT( InDet::PixelClusterOnTrackTool )
DECLARE_COMPONENT( InDet::SCT_ClusterOnTrackTool )
DECLARE_COMPONENT( ITk::PixelClusterOnTrackTool )
